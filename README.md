Mark Feaver - 1013657

### README ###

To setup and run the shell, simply type "make". Then, type "shell".

The machine name + working directory is displayed on each line, followed by a $. Users may enter commands after this.

Special command: "greet", following by space separated names.
e.g. greet Mark Feaver

Can run multiple commands by separating by a ;
e.g. ls -1; mkdir test; ls -1

To exit the shell, either press CTRL+D or type "exit".

ENJOY!!!
