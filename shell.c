#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

/** A simple shell program.
 ** Created by Mark Feaver, 2012
 **/
int main(int argc, char *argv[]){
	//-----class variables-----
	char *args[256];
	char *commands[256];
	char input[10000];
	char in_split[10000];
	char *delim = " ";
	int count = 0;
	int error_code;
	char *exit_str = "exit";
	char l = '$';
	char dir[1000];
	char host_name[1000];
	
	//get the machine name	
	gethostname(host_name, 1000);
	//get the current working directory
	getcwd(dir, 1000);
		
	//the pid of the program being run by the shell
	pid_t pid;

	//print the initial shell command line shiz
	printf("%s:%s%c",host_name, dir, l);
	
	//while there is still input
	while (fgets(input, sizeof(input), stdin)){		
		//prune the /n off the last string
		input[strlen(input)-1] = '\0'; 
		if (input[0] != '\0'){
			//split input at each space character
			count = splitline(&commands, input, ";");

			int j = 0;
			for (j; j < count; j++){			
				
				//check for exit()
				if (strncmp(commands[j], exit_str, 4) == 0){
					return 0;
				}
				
				//fork
				pid = fork();
				
				//if failure
				if (pid == -1){
					printf("Failed to fork\n");
					exit(1);
				}

				//if child
				if (pid == 0){
					splitline(&args, commands[j], " ");
					
					//handles the greet function
					if (strcmp(args[0], "greet") == 0){
						printf("Hello,");
						int gk = 1;
						while (args[gk] != NULL){
							printf(" %s", args[gk]);
							gk++;
						}
						printf("!\n");
						_exit(0);
					}

					//execute the command line
					error_code = execvp(args[0], args);
					
					if (error_code == -1){
						printf("Command not found: %s\n", args[0]);
					}
					_exit(0);
				}
				
				//if parent
				else {
					int exit_status;
					pid_t ws = waitpid(pid, &exit_status, 0);
		
					if (!WIFEXITED(exit_status)){
						printf("Child exited due to an error: %d\n", exit_status);
					}
				}
			}
			
			count = 0;
		}
		printf("%s:%s%c",host_name,dir,l);
	}
}

/**
 * A helper method to split up a given command, based on a certain delimiter.
 * Stores the result in *inputargs[]
 **/
int splitline(char *inputargs[], char commandline[], char delimiter[]){
	char *token = NULL;
	int counter = 0;
	for (token = strtok(commandline, delimiter); token; token = strtok(NULL, delimiter)){
		inputargs[counter] = token;
		counter++;
	}
	inputargs[counter] = NULL;
	
	return counter;
}
